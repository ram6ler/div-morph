import 'dart:html';

void main() {
  /// The text for the show and hide buttons.
  String show, hide;
  {
    /// Defualt text for the show and hide buttons.
    final defaultShow = 'I have thought about it - show me!',
        defaultHide = 'I want to think about this again - hide!';

    // Get the settings from div with id="peek-a-boo-settings".
    final settings = querySelector('#peek-a-boo-settings'),
        text = settings?.text;

    if (text == null) {
      show = defaultShow;
      hide = defaultHide;
    } else {
      String extract(String setting) {
        final start = setting.indexOf('"'),
            end = setting.indexOf('"', start == -1 ? 0 : start + 1);
        if (start == -1 || end == -1) {
          return null;
        } else
          return setting.substring(start + 1, end);
      }

      for (final setting in text.split(';')) {
        if (setting.contains('show-message')) {
          show = extract(setting) ?? defaultShow;
        } else if (setting.contains('hide-message')) {
          hide = extract(setting) ?? defaultHide;
        }
      }

      // Remove the settings div.
      settings.style.display = "none";
    }
  }

  // Add peek-a-boo functionality to all divs with class="peek-a-boo".
  for (final div in querySelectorAll('.peek-a-boo')) {
    div.style.visibility = 'hidden';
    ButtonElement button;
    button = ButtonElement()
      ..innerText = show
      ..onClick.listen((_) {
        if (toggle(div)) {
          button.innerText = hide;
        } else {
          button.innerText = show;
        }
      });
    div.before(button);
  }
}

/// Toggles a div's visibility setting.
bool toggle(Element div) {
  if (div.style.visibility == 'hidden') {
    div.style.visibility = 'visible';
    return true;
  } else {
    div.style.visibility = 'hidden';
    return false;
  }
}
